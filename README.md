# Настройки по умолчанию #

Чтобы сделать привычные мне настройки на любом linux-компьютере:
	
	apt-get update && apt-get upgrade && apt-get install -y zsh grc tmux git curl sudo vim wget
	wget -qO- https://bitbucket.org/bsnjoy/configs/raw/master/initconfigs.sh | sh

**zsh** - надо для oh-my-zsh, **grc** - для раскраски многих команд (alias в .zshrc)
	
	# Если не под root, то пепрая строчка:
	sudo apt-get update && sudo apt-get upgrade && sudo apt-get install -y zsh grc tmux git curl vim wget

### Какие настройки включены? ###

* .vimrc и цветовая схема desert256
* Настройка oh-my-zsh (файл .zshrc)
* Сам файл initconfigs.sh который делает автоматическое развертывание настроек
* .gitconfig - с указанием автора и e-mail для git коммитов по умолчанию

### Как делать доработки ###
В файле .zshrc определен
	alias gg='git --git-dir=.configsgit --work-tree=.'
Благодаря кототорому можно просто использовать вместо git комманду gg и она автоматически установит переменные окружения git для этих настроек
