#!/usr/bin/env bash

cd ~

# install oh-my-zsh
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | bash
echo "We will now change shell to zsh, please enter current user password:"
chsh -s $(which zsh) $(whoami)

### OLD STYLE INIT
#### Удалим конфиг, чтобы файл из репозитория без вопросов заменился
###rm  ~/.zshrc
###git clone --no-checkout https://bitbucket.org/bsnjoy/configs.git
###git --git-dir=configs/.git --work-tree=. reset --hard origin/master

cd ~
git --git-dir=.configsgit init
git --git-dir=.configsgit remote add origin https://bitbucket.org/bsnjoy/configs.git
git --git-dir=.configsgit fetch --all --prune
git --git-dir=.configsgit --work-tree=. checkout -f master

# This is not very secure, but I don't like write "yes" everytime
#if [ ! -n "$(grep "^bitbucket.org " ~/.ssh/known_hosts)" ]; then ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts 2>/dev/null; fi
#git clone --no-checkout git@bitbucket.org:bsnjoy/configs.git
# cd configs
# git config core.worktree "../../"
# force git to overwrite our current files
# git reset --hard origin/master
# Раскраска вывода git
git config --global color.ui true
# https://lostechies.com/keithdahlby/2011/04/06/windows-git-tip-hide-carriage-return-in-diff/
# Checkout as-is, commit Unix-style
git config --global core.autocrlf input
git config --global core.whitespace cr-at-eol
git config --global user.name "Stepan Da"
git config --global user.email "bsnjoy@gmail.com"

echo "Use alias 'gg' instead of 'git' to view commit and push changes"
