# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="robbyrussell"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Uncomment this to disable bi-weekly auto-update checks
DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment following line if you want to  shown in the command execution time stamp 
# in the history command output. The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|
# yyyy-mm-dd
# HIST_STAMPS="mm/dd/yyyy"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

export PATH="/usr/local/bin:/opt/local/bin:/opt/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin"
export PATH="/usr/local/mysql/bin:$PATH"
# export MANPATH="/usr/local/man:$MANPATH"

# # Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# alias -g gr="| grep"
# alias -g l="| less"

hash -d buy=/home/a/work/docs/buy


# Данный конфиг доставит нам комплит для комманд kill и killall
zstyle ':completion:*:processes' command 'ps -ax' 
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;32'
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*'   force-list always

zstyle ':completion:*:processes-names' command 'ps -e -o comm='
zstyle ':completion:*:*:killall:*' menu yes select
zstyle ':completion:*:killall:*'   force-list always

# Раскраска man
export LESS_TERMCAP_mb=$'\E[01;33m' # цвет мерцающего стиля
export LESS_TERMCAP_md=$'\E[01;31m' # цвет полужирного стиля
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;42;30m' # цвет и фон служебной информации
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m' # цвет подчеркнутого стиля
export LESS_TERMCAP_ue=$'\E[0m'

# цвет выделения для grep
export GREP_COLOR="1;31"

# Раскраска ls
export LS_COLORS='no=00;37:fi=00;37:di=01;36:ln=04;36:pi=33:so=01;35:do=01;35:bd=33;01:cd=33;01:or=31;01:su=37:sg=30:tw=30:ow=34:st=37:ex=01;31:*.cmd=01;31:*.exe=01;31:*.com=01;31:*.btm=01;31:*.sh=01;31:*.run=01;31:*.tar=33:*.tgz=33:*.arj=33:*.taz=33:*.lzh=33:*.zip=33:*.z=33:*.Z=33:*.gz=33:*.bz2=33:*.deb=33:*.rpm=33:*.jar=33:*.rar=33:*.jpg=32:*.jpeg=32:*.gif=32:*.bmp=32:*.pbm=32:*.pgm=32:*.ppm=32:*.tga=32:*.xbm=32:*.xpm=32:*.tif=32:*.tiff=32:*.png=32:*.mov=34:*.mpg=34:*.mpeg=34:*.avi=34:*.fli=34:*.flv=34:*.3gp=34:*.mp4=34:*.divx=34:*.gl=32:*.dl=32:*.xcf=32:*.xwd=32:*.flac=35:*.mp3=35:*.mpc=35:*.ogg=35:*.wav=35:*.m3u=35:';

alias mutt="cd ~/Downloads && mutt"
alias ll='grc ls -lFh --color=yes'
alias ping="grc ping"
alias traceroute="grc traceroute"
alias make="grc make"
alias diff="grc diff"
alias cvs="grc cvs"
alias netstat="grc netstat"
alias logc="grc cat"
alias logt="grc tail"
alias logh="grc head"

alias nmap='grc nmap'
alias cdtpl='cd catalog/view/theme/default/template/'
alias cdcss='cd catalog/view/theme/default/stylesheet/'
alias cde='cd /Users/s/www/ebong.ru'
alias cdj='cd /Users/s/www/jigi.ru'
alias cdw='cd /Users/s/www/'
alias cdk='cd /Users/s/www/kalyanov.net'
alias cdf='cd /Users/s/www/futuresmoking.ru'
alias w1='vim ~/Dropbox/vimwiki/main/index.wiki'
alias w5='vim ~/Dropbox/vimwiki/health/index.wiki'
alias w2='/Users/s/Dropbox/vimwiki/wiki.ilfumo.ru/html && vim ../index.wiki'
alias c2='cd /Users/s/Dropbox/vimwiki/wiki.ilfumo.ru/ && git status'
alias w3='vim ~/Dropbox/vimwiki/learn/index.wiki'
alias vs='vim ~/.ssh/config'
alias dl='rm ~/Dropbox/pass.kdb.lock'
# To prevent zsh from asking confirmations before delete, run:
# setopt rmstarsilent
alias ds='bash -c "rm -f ~/.ssh/cm_socket/*" && echo "deleteted all files from ~/.ssh/cm_socket/"'
alias ms='sudo /usr/local/mysql/support-files/mysql.server start'
alias tt='~/cmd/telnetstart.sh'
alias t='todo.sh'
alias ta='todo.sh add'
alias u='rsnapshot manual && cd ~/.snapshots/manual.0/img/home/pi/img && ls -l | wc -l'
#alias mc='export LANG=en_US.utf8; mc'
#alias s='echo hi'

tell () { echo $1 | sendxmpp stepa@delatel.com }

# stackoverflow.com/questions/221921/use-grep-exclude-include-syntax-to-not-grep-through-certain-files
# -i  Ignore case
#gre (){ grep -r -n -i --exclude="*{\.sql,\/cache\/*,uk\.js}" $1 . }
gre (){ grep -r -n -i --exclude="*uk.js" --exclude="*.sql" --exclude="*\/cache\/*" $1 . }
# This works best for me on OS X:
# grep -r -l 'searchtext' . | sort | uniq | xargs perl -e "s/matchtext/replacetext/" -pi
grere () { grep -rl --exclude="*uk.js" --exclude="*.sql" --exclude="*\/cache\/*" $1 . | xargs perl -e "s/$1/$2/" -pi }

f (){ find . -iname "*$1*" }
p (){ lp -d EPSON_L350_Series_2 $1 }

# PNG to JPG convertion with diffreent quality levels
pj100 (){ convert $1 -trim -strip -bordercolor White -alpha remove -verbose `echo $1 | sed 's/\.png/\.jpg/'` }
pj90 (){ convert $1 -trim -strip -bordercolor White -alpha remove -verbose -quality 90 `echo $1 | sed 's/\.png/\.jpg/'` }
pj80 (){ convert $1 -trim -strip -bordercolor White -alpha remove -verbose -quality 80 `echo $1 | sed 's/\.png/\.jpg/'` }
pj75 (){ convert $1 -trim -strip -bordercolor White -alpha remove -verbose -quality 75 `echo $1 | sed 's/\.png/\.jpg/'` }
pj70 (){ convert $1 -trim -strip -bordercolor White -alpha remove -verbose -quality 70 `echo $1 | sed 's/\.png/\.jpg/'` }
pj65 (){ convert $1 -trim -strip -bordercolor White -alpha remove -verbose -quality 65 `echo $1 | sed 's/\.png/\.jpg/'` }
pj60 (){ convert $1 -trim -strip -bordercolor White -alpha remove -verbose -quality 60 `echo $1 | sed 's/\.png/\.jpg/'` }
 
# PNG compress
png6 (){ convert $1 -strip -interlace none +dither -colors 6 $1; optipng -o7 $1 }
png (){ convert $2 -strip -interlace none +dither -colors $1 $1$2; optipng -o7 $1$2 }
pngall (){
COLORS=6
convert $1 -strip -interlace none +dither -colors $COLORS $COLORS$1; optipng -o7 $COLORS$1
COLORS=10
convert $1 -strip -interlace none +dither -colors $COLORS $COLORS$1; optipng -o7 $COLORS$1
COLORS=20
convert $1 -strip -interlace none +dither -colors $COLORS $COLORS$1; optipng -o7 $COLORS$1
COLORS=40
convert $1 -strip -interlace none +dither -colors $COLORS $COLORS$1; optipng -o7 $COLORS$1
COLORS=60
convert $1 -strip -interlace none +dither -colors $COLORS $COLORS$1; optipng -o7 $COLORS$1
COLORS=100
convert $1 -strip -interlace none +dither -colors $COLORS $COLORS$1; optipng -o7 $COLORS$1
COLORS=255
convert $1 -strip -interlace none +dither -colors $COLORS $COLORS$1; optipng -o7 $COLORS$1
COLORS=1024
convert $1 -strip -interlace none +dither -colors $COLORS $COLORS$1; optipng -o7 $COLORS$1
COLORS=4096
convert $1 -strip -interlace none +dither -colors $COLORS $COLORS$1; optipng -o7 $COLORS$1
}
il (){ scp $1 ilfumo:/var/www/ilfumo.ru/httpdocs/wp-content/uploads/ }
export PATH=/usr/local/sbin:/Users/s/Dropbox/cmdsh:$PATH


#
# Russian Fix
#
export LC_CTYPE="ru_RU.UTF-8"
export LANG="ru_RU.UTF-8"
# disable ._ files when creating tar archive
export COPYFILE_DISABLE=true
# this is to use vim 7.4 in MacOSX instead of system 7.3
#alias vim ='/usr/local/bin/vim'
# команде в терминале qq картинка из буфера обмена сохраняется в директорию с wiki и ссылка в формате для vimwiki сохраняется в буфер обмена. Во втором случае надо указать имя файла для сохранения
alias qq='IMG=~/Dropbox/vimwiki/main/images/img`date +%s`.png && pngpaste $IMG && echo {{$IMG}}|pbcopy'
q () { IMG=~/Dropbox/vimwiki/main/images/$1.png && pngpaste $IMG && echo {{$IMG}}|pbcopy }
www() { HTML=~/Dropbox/vimwiki/main/postavki/www`date +%s`.html && osascript -e 'the clipboard as «class HTML»' | perl -ne 'print chr foreach unpack("C*",pack("H*",substr($_,11,-3)))' >> $HTML && echo {{$HTML}}|pbcopy }
# w() { HTML=~/Dropbox/vimwiki/main/postavki/$1.html && osascript -e 'the clipboard as «class HTML»' | perl -ne 'print chr foreach unpack("C*",pack("H*",substr($_,11,-3)))' > $HTML }
co() { git commit -a -m $1 $2 $3 $4 $5 $6 $7 $8 $9 $10 }

# Вывод текущих задач todo
# t
# Ближайшие 3 дня рождения и события
~/Dropbox/cmdsh/contacts_birthday.sh
export PATH="/usr/local/opt/openssl/bin:$PATH"
#cd ~/Downloads
alias jjj="pkill joxi"
alias delatel="ssh delatel"

# Add autocomplition from ~/.ssh/config_mikrotik
# https://unix.stackexchange.com/questions/52099/how-to-append-extend-zshell-completions
zstyle -s ':completion:*:hosts' hosts _ssh_config
[[ -r ~/.ssh/config ]] && _ssh_config+=($(cat ~/.ssh/config_mikrotik | sed -ne 's/Host[=\t ]//p'))
zstyle ':completion:*:hosts' hosts $_ssh_config
gre() {grep --include \*.php --include \*.tpl -rn $1 .}
alias v='vim $(fzf)'


tosmall() { find . -name "*.jpg" -print0 | xargs -0 -t -I % sh -c 'convert "%" -resize 1024x1024 -trim -strip -alpha remove -verbose -quality 90 "%"' }

alias gg='git --git-dir=.configsgit --work-tree=.'

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

alias exit='clear'
alias logout='clear'
tmux attach || tmux new
clear
