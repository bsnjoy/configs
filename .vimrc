"Вставить из буфера обмена
"nmap <C-v> :r! xclip -o<CR>  
"nmap <C-c> :'<,'>w !xclip
"set keymap=russian-jcukenwin "переключение на русский через ctrl-^
"set iskeyword=@,48-57,_,192-255 "учим вим правильно понимать русские слова (перемещение по тексту)
"set langmap=!№\\;%?*ёйцукенгшщзхъфывапролджэячсмитьбюЁЙЦУКЕГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ.;!#$%&*`qwertyuiop[]asdfghjkl\\;'zxcvbnm\\,.~QWERTUIOP{}ASDFGHJKL:\\"ZXCVBNM<>/
" Все русские буквы кроме : переделываем в английские в коммандном режиме. Убрано соответсвтие НY - по непонятным причинам оно блокирет действия = и - в wiki, убрал \\" -> @.
"!"№;%?*ёйцукенгшщзхъфывапролджэячсмитьбюЁЙЦУКЕHГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ;
"!@#$%&*`qwertyuiop[]asdfghjkl;'zxcvbnm\\,.~QWERTYUIOP{}ASDFGHJKL:\\"ZXCVBNM<>
" Для vimwiki
"imap № #
"set langmap=!\\"#\\;%:?*ёйцукенгшщзхъфывапролджэячсмитьбюЁЙЦУКЕHГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ;!@#$%^&*`qwertyuiop[]asdfghjkl\\;'zxcvbnm\\,.~QWERTYUIOP{}ASDFGHJKL:\\"ZXCVBNM<>
"set iminsert=0  " по умолчанию - латинская раскладка
"set imsearch=0  " по умолчанию - латинская раскладка при поиске
set incsearch   " автоматически показывать первое совпадение при поиске
set hlsearch    " подсвечивать жёлтым все совпадения с шаблоном поиска
set ignorecase  " Игнорировать регистр бугв при поиске
colorscheme desert256   " Устанавливаем цветовую схему
set encoding=utf8
set nocompatible " Отключаем поддержку vi. Нужно для vimwiki
filetype plugin on

"Вырубаем .swp и ~ (резервные) файлы
set nobackup
set nowritebackup
set noswapfile

" Настройка отступов
set tabstop=4
set shiftwidth=4
"set smarttab
"set expandtab "Ставим табы пробелами
set autoindent "копирует отступы с текущей строки при добавлении новой.

"Подсвечиваем все что можно подсвечивать
let python_highlight_all = 1
"Включаем 256 цветов в терминале, мы ведь работаем из иксов?
set t_Co=256

"Настройка omnicomletion для Python (а так же для js, html и css)
"autocmd FileType python set omnifunc=pythoncomplete#Complete
"autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
"autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
"autocmd FileType css set omnifunc=csscomplete#CompleteCSS

"найти визуально выделенный текст
"vmap // y/<C-R>"<CR>
" Сохранение и запуск программы
"nmap <F5> :w<CR>:! ./%<CR>
nmap <F5> :w<CR>:! open ./%<CR>

set nospell " По умолчанию орфографию не проверяем

" vimwiki with markdown support
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
" helppage -> :h vimwiki-syntax 

" vim-instant-markdown - Instant Markdown previews from Vim
" https://github.com/suan/vim-instant-markdown
let g:instant_markdown_autostart = 0	" disable autostart
map <leader>md :InstantMarkdownPreview<CR>

" Убрать подсветку по ESC
":nnoremap <F4> :noh<return><esc>

:command NA set noautoindent
:command A set autoindent
" Это строчка чтобы backspace работал на все символы а не только те что
" напечатаны в этой сессии
set backspace=indent,eol,start

set laststatus=2   " всегда показывать строку статуса
set statusline=%F%m%r%h%w\ %y\ enc:%{&enc}\ ff:%{&ff}\ fenc:%{&fenc}%=(ch:%3b\ hex:%2B)\ col:%2c\ line:%2l/%L\ [%2p%%]
" %F - will be the full path   set statusline+=%F
" %f - имя файла и путь к нему, относительно текущего каталога
" %m - флаг модификации/изменения, выводит [+] если буфер изменялся
" %r - флаг "только для чтения", выводит [RO] если буфер только для чтения
" %h - флаг буфера помощи, выводит [help] если буфер со справкой vim
" %w - флаг окна превью, выводит [Preview]
" '\ ' - экранированный символ пробела. Пробел можно указывать только экранированным, иначе ошибка синтаксиса
" %y - тип файла в буфере, например [vim]
" enc:%{&enc} - отображение кодировки encoding (enc). Обратите внимание: "enc:" - текст, "%{&enc}" - вывод значения внутренней переменной (enc)
" ff:%{&ff} - отображение формата перевода строки fileformat (ff)
" fenc:%{&fenc} - отображение кодировки сохранения в файл fileencoding (fenc)
" %= - далее выравнивать вправо
" ch:%3b - код символа под курсором в десятичной чистеме счисления, минимум 3 символа
" hex:%2B - код символа под курсором в шестнадцатеричной системе счисления, минимум 2 символа
" col:%2c - курсор в колонке, минимум 2 символа
" line:%2l/%L - курсор в строке (минимум 2 символа)/всего строк в файле
" %2p - число % в строках, где находится курсор (0% - начало файла; 100% - конец файла), минимум 2 символа
" %% - т.к. символ '%' используется для переменных, то вывод символа '%' в строке статуса нужно делать особым образом - %%

"set listchars=tab:→→,trail:·,nbsp:↔,extends:>,precedes:<
"set list
set nolist  " list disables linebreak

set number
map <F2> :set invnumber<CR>
nmap <F2> :set invnumber<CR>
"nmap <F8> iplaced on <C-R>=strftime("%d.%m.%Y")<CR><Esc>
"imap <F8> placed on <C-R>=strftime("%d.%m.%Y")<CR>
"imap <F8> Attach:/Users/s/Dropbox/work/price/favorites/ilfumo_distribution_terms.pdf<Esc>:r !cat /Users/s/.vim/templates/ilfumo_distribution_terms.skeleton<CR>

" bind Ctrl+<movement> keys to move around the windows, instead of using Ctrl+w + <movement>
" Every unnecessary keystroke that can be saved is good for your health :)
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" При редактировании письма в mutt курсор сразу перемещается на следущее поле
" после заголовков и переходит в режим insert
"autocmd BufRead /Users/s/.mutt/temp/mutt-*  execute "normal /^$\n"
"autocmd BufRead /Users/s/.mutt/temp/mutt-*  execute "normal gg/\n\n<return>O"
"autocmd BufRead /Users/s/.mutt/temp/mutt-*  execute ":startinsert"

" setlocal fo+=aw

set wrap
set linebreak
set textwidth=0
" http://stackoverflow.com/questions/2280030/how-to-stop-line-breaking-in-vim
set wrapmargin=0
" http://blog.ezyang.com/2010/03/vim-textwidth/  text wrapping turn off next line  wow line
set fo-=t

"call plug#begin('~/.vim/plugged')
"Plug 'Valloric/YouCompleteMe', { 'do': './install.py --tern-completer' }
"call plug#end()
" Закрытие html тэгов в режиме insert gj по нажатию ctrl+пробел
:imap <C-Space> </<C-X><C-O><Esc>==gi

:set paste

" Восстановление позиции курсора при открытии файла в Vim + фолдинги восстановить.
" ? перед * - убрать совпадение с пустым именем файла. (при открытии vim без файла чтобы не было ошибок)
if has("autocmd")
	set viewoptions=cursor,folds
	au BufWinLeave ?* mkview
	au BufWinEnter ?* silent loadview
endif

"#execute pathogen#infect()
syntax on
filetype plugin indent on
"https://stackoverflow.com/questions/6991638/how-to-auto-save-a-file-every-1-second-in-vim
"autocmd TextChanged,TextChangedI <buffer> silent write
